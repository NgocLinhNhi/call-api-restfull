package call.api.project.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Customer {
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private int age;

    //đẻ constructor này trong entiy là oẳng ngay convert kiểu response trả về từ api
//    public Customer(String name, int age) {
//        this.name = name;
//        this.age = age;
//    }
}
