package call.api.project.constant;

public class ProvideConstant {
    public static final long TIME_MILS_LONG = 3000;

    public static final String APPLICATION_PROPERTIES = "application.properties";
    public static final String CUSTOMER_URI_POJO = "get.customer.url.pojo";
    public static final String CUSTOMER_URI = "get.customer.url";
    public static final String CUSTOMER_BY_NAME_URI = "get.customer.url.customer.by.name";
    public static final String CUSTOMER_BY_NAME_GET_METHOD_REQUEST_PARAM = "get.customer.url.customer.by.param";
    public static final String CUSTOMER_BY_NAME_POST_METHOD_REQUEST_BODY = "get.customer.url.customer.by.request.body";
}
