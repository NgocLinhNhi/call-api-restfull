package call.api.project.handler;

import call.api.project.config.AppConfig;
import call.api.project.config.RestTemplateConfig;
import call.api.project.entity.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.Objects;

@Controller
public class CustomerHandler {

    private Logger logger = LoggerFactory.getLogger(RestTemplateConfig.class);

    private static CustomerHandler INSTANCE;
    private RestTemplateConfig restTemplateConfig;

    public static CustomerHandler getInstance() throws IOException {
        if (INSTANCE == null) INSTANCE = new CustomerHandler();
        return INSTANCE;
    }

    private CustomerHandler() throws IOException {
        restTemplateConfig = new RestTemplateConfig();
    }

    public String getCustomerRestTemplate() {
        String customerResponse = restTemplateConfig.restApiHandler();
        logger.info("Response {}", customerResponse);
        return customerResponse;
    }

    public Customer getCustomerPOJO() {
        Customer customerResponse = restTemplateConfig.httpGetPOJOTemplate();
        logger.info("Response {}", customerResponse.getName());
        return customerResponse;
    }

    public ResponseEntity<Customer> getCustomerForEntity() {
        String customerUriPOJO = AppConfig.getCustomerUriPOJO();
        ResponseEntity<Customer> customerResponseEntity = restTemplateConfig.httpGetEntityTemplate(customerUriPOJO);

        logger.info("Response Body {}", Objects.requireNonNull(customerResponseEntity.getBody()).getName());
        return customerResponseEntity;
    }

    public Customer getCustomerByNamePathVariable() {
        Customer customerResponse = null;
        try {
            String customerByNameUri = AppConfig.getCustomerByNameUri();
            customerResponse = restTemplateConfig.restApiGetMethodPathVariable(customerByNameUri, "Viet");
            logger.info("Response {}", customerResponse.getName());
        } catch (Exception ex) {
            logger.error("CUSTOMER NOT FOUND {1}", ex);
        }
        return customerResponse;
    }

    public Customer getCustomerByGetMethodRequestParam() {
        logger.info("CALL API WITH GET METHOD REQUEST PARAM");

        Customer customerResponse;
        String customerByNameUriBody = AppConfig.getCustomerByNameGetMethodRequestParam();
        customerResponse = restTemplateConfig.restFulGetMethodRequestParam(customerByNameUriBody, customerRequest());

        if (customerResponse != null) {
            logger.info("Response {}", customerResponse.getName());
        } else {
            logger.info("CUSTOMER NOT FOUND");
        }

        return customerResponse;
    }

    public Customer getCustomerByPostMethodRequestBody() {
        Customer customerResponse;
        logger.info("CALL API WITH POST METHOD REQUEST BODY");

        String customerByNameUriBody = AppConfig.getCustomerByNamePostMethodRequestBody();
        customerResponse = restTemplateConfig.restFulPostMethodRequestBody(customerByNameUriBody, customerRequest());

        if (customerResponse != null) {
            logger.info("Response {}", customerResponse.getName());
        } else {
            logger.info("CUSTOMER NOT FOUND");
        }

        return customerResponse;
    }

    private Customer customerRequest() {
        Customer customerRequest = new Customer();
        customerRequest.setName("Viet");
        customerRequest.setAge(50);
        return customerRequest;
    }
}
