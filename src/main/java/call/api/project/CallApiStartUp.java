package call.api.project;

import call.api.project.handler.CustomerHandler;

import java.io.IOException;

public class CallApiStartUp {
    public static void main(String[] args) throws IOException {
        CustomerHandler instance = CustomerHandler.getInstance();
        //instance.getCustomerRestTemplate();
        //instance.getCustomerPOJO();
        instance.getCustomerForEntity();
        //instance.getCustomerByNamePathVariable();
        //instance.getCustomerByPostMethodRequestBody();
        // instance.getCustomerByGetMethodRequestParam();
    }
}
