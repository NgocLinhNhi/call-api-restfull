package call.api.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CalApiApplication.class, args);
    }

}
