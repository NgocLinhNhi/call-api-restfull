package call.api.project.connector;

import org.apache.http.impl.client.CloseableHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

import static call.api.project.constant.ProvideConstant.TIME_MILS_LONG;

public class SSOConnector {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {

        return builder
                .setConnectTimeout(Duration.ofMillis(TIME_MILS_LONG))
                .setReadTimeout(Duration.ofMillis(TIME_MILS_LONG))
                .build();
    }

//    @Bean
//    public RestTemplate restTemplate() {
//
//        var factory = new SimpleClientHttpRequestFactory();
//        factory.setConnectTimeout(TIME_MILS_INT);
//        factory.setReadTimeout(TIME_MILS_INT);
//        return new RestTemplate(factory);
//    }


    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate(clientHttpRequestFactory());
    }

    @Autowired
    CloseableHttpClient httpClient;

    @Bean
    public HttpComponentsClientHttpRequestFactory clientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setHttpClient(httpClient);
        return clientHttpRequestFactory;
    }
}
