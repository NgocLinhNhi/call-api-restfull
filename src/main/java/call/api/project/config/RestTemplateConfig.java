package call.api.project.config;

import call.api.project.entity.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class RestTemplateConfig {

    private Logger logger = LoggerFactory.getLogger(RestTemplateConfig.class);

    public RestTemplateConfig() throws IOException {
        AppConfig.getInstance().loadProperties();
        logger.info("LOADING CONFIG DONE !!!!!!!");
    }

    public String restApiHandler() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(AppConfig.getCustomerUri(), String.class);
    }

    public Customer httpGetPOJOTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(AppConfig.getCustomerUriPOJO(), Customer.class);
    }

    public ResponseEntity<Customer> httpGetEntityTemplate(String customerUriPOJO) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-LOCATION", "USA");
        headers.set("X-COM-PERSIST", "NO");
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>(headers);
        return restTemplate.exchange(customerUriPOJO, HttpMethod.GET, entity, Customer.class);
    }

    //GET METHOD PATH VARIABLE
    public Customer restApiGetMethodPathVariable(String customerUriPOJO, String name) {
        System.out.println("CALLING API BY GET METHOD PATHVARIABLE");
        RestTemplate restTemplate = new RestTemplate();

        Map<String, String> params = new HashMap<>();
        params.put("name", name);
        return restTemplate.getForObject(customerUriPOJO, Customer.class, params);
    }

    //GET METHOD REQUEST PARAM
    public Customer restFulGetMethodRequestParam(String customerUriPOJO, Customer customerRequest) {
        logger.info("CALLING API BY GET METHOD REQUEST PARAM");
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(customerUriPOJO, Customer.class, customerRequest.getName());
    }

    //POST METHOD - REQUEST BODY
    public Customer restFulPostMethodRequestBody(String customerUriPOJO, Customer customer) {
        logger.info("CALLING API BY POST METHOD REQUEST BODY");
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForObject(customerUriPOJO, customer, Customer.class);
    }

}
