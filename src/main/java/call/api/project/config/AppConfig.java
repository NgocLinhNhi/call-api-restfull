package call.api.project.config;

import call.api.project.io.InputStreams;
import lombok.Getter;

import java.io.IOException;
import java.util.Properties;

import static call.api.project.constant.ProvideConstant.*;

public class AppConfig {
    //bình thường project SpringBoot run file application.run thì sẽ tự lấy đc value trong file application.properties dễ dàng = @Value
    //Tuy nhiên chạy run hàm main bt = test sẽ không lấy được value ( dùng khi unit test đâu -> nên lấy value trong file application.properties kiểu này

    private static AppConfig instance;

    @Getter
    private static String customerUriPOJO;
    @Getter
    private static String customerUri;
    @Getter
    private static String customerByNameUri;
    @Getter
    private static String customerByNameGetMethodRequestParam;
    @Getter
    private static String customerByNamePostMethodRequestBody;

    static synchronized AppConfig getInstance() {
        if (instance == null) {
            instance = new AppConfig();
        }
        return instance;
    }

    void loadProperties() throws IOException {
        Properties properties = new Properties();
        properties.load(InputStreams.getInputStream(APPLICATION_PROPERTIES));

        customerUriPOJO = properties.getProperty(CUSTOMER_URI_POJO);
        customerUri = properties.getProperty(CUSTOMER_URI);
        customerByNameUri = properties.getProperty(CUSTOMER_BY_NAME_URI);
        customerByNamePostMethodRequestBody = properties.getProperty(CUSTOMER_BY_NAME_POST_METHOD_REQUEST_BODY);
        customerByNameGetMethodRequestParam = properties.getProperty(CUSTOMER_BY_NAME_GET_METHOD_REQUEST_PARAM);
    }
}
