package call.api.project.io;

import java.io.InputStream;

public interface InputStreamFetcher {

    InputStream getInputStream(String filePath);

}
